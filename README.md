# Matchutils

**NOTE:** *`matchutils` is still a work in progress, and as such, it should be
considered alpha software. The API is subject to change without warning. When
the API is considered stable, v1 will be released. Until that point, please
exercise due caution.*

`matchutils` provides extensions for python's `match`/`case` statement.

```python
"""example.py"""

from matchutils import AsRegex

match AsRegex('hello world'):
    case '^h.*':
        print('foo')
    case _:
        print('bar')
```

```
$ python3 example.py
foo
```

## Limitations

The classes that `matchutils` provides only work with
[value patterns](https://peps.python.org/pep-0634/#value-patterns).
I reccommend reading about
[the different kinds of patterns](https://peps.python.org/pep-0634/#patterns)
in [PEP 634](https://peps.python.org/pep-0634/) for a clearer understanding of
why this is the case.
