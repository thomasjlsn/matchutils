#!/usr/bin/env python3


from matchutils import AsPattern


test_cases = (
    ('hello', 'h*'),
    ('hello world', 'h*d'),
    ('hello world', 'h??lo **d'),
)


for string, pattern in test_cases:
    fail = False

    class C:
        pattern = pattern

    match AsPattern(string):
        case C.pattern:
            ...
        case _:
            fail = True

    if fail:
        raise AssertionError(f'AsPattern: "{pattern}" should match "{string}"')
