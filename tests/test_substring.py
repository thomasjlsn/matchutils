#!/usr/bin/env python3


from matchutils import AsSubstring


test_cases = (
    ('hello', 'hell'),
    ('hello world', 'llo'),
    ('hello world', 'world'),
)


for string, pattern in test_cases:
    fail = False

    class C:
        pattern = pattern

    match AsSubstring(string):
        case C.pattern:
            ...
        case _:
            fail = True

    if fail:
        raise AssertionError(f'AsSubstring: "{pattern}" should match "{string}"')

