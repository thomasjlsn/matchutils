install: test
	pip install --user .

install-editable: test
	pip install --user -e .

deploy: build-dist
	twine upload dist/*

deploy-testing: build-dist
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

build-dist: clean test
	python3 setup.py sdist bdist_wheel
	twine check dist/*

clean:
	rm -rfv build/
	rm -rfv dist/
	rm -rfv *.egg-info/

test:
	find tests/ -name test*.py | xargs -n1 python
