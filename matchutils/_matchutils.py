'''matchutils: extensions for python's match/case statement

https://github.com/thomasjlsn/matchutils
'''

import re
from fnmatch import translate


# HACK (this applies to all following `__eq__` methods):
#      mypy expects `other` in `__eq__()` signature to be type `object`, but we
#      need it to be type `str`. Oh well, Extra type safety is ok with me.


def _init_type_error(obj: object) -> str:
    return f'the first argument to "{obj.__class__.__name__}" must be type "str"'


def _eq_type_error(obj: object) -> str:
    return f'case arguments against "{obj.__class__.__name__}" must be type "str"'


class AsSubstring:
    def __init__(self, value: str, case_insensitive: bool = False) -> None:
        if not isinstance(value, str):
            raise TypeError(_init_type_error(self))

        self.case_insensitive: bool = case_insensitive
        self.value: str = value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, str):
            raise TypeError(_eq_type_error(self))

        if self.case_insensitive:
            return self.value.casefold().__contains__(other.casefold())
        else:
            return self.value.__contains__(other)


class AsRegex:
    def __init__(self, value: str, case_insensitive: bool = False) -> None:
        if not isinstance(value, str):
            raise TypeError(_init_type_error(self))

        self.case_insensitive: bool = case_insensitive
        self.value: str = value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, str):
            raise TypeError(_eq_type_error(self))

        if self.case_insensitive:
            return bool(re.search(other, self.value, flags=re.I))
        else:
            return bool(re.search(other, self.value))


class AsPattern:
    def __init__(self, value: str, case_insensitive: bool = False) -> None:
        if not isinstance(value, str):
            raise TypeError(_init_type_error(self))

        self.case_insensitive: bool = case_insensitive
        self.value: str = value

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, str):
            raise TypeError(_eq_type_error(self))

        if self.case_insensitive:
            return bool(re.compile(translate(other), flags=re.I).search(self.value))
        else:
            return bool(re.compile(translate(other)).search(self.value))
