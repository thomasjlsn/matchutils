'''matchutils: extensions for python's match/case statement


matchutils.AsPattern

    compare case items using shell pattern matching syntax

    >>> match AsPattern('hello world'):
    ...     case 'he*d':
    ...         print('foo')
    ...
    foo

matchutils.AsRegex

    compare case items using regular expressions

    >>> match AsRegex('hello world'):
    ...     case 'hel+o ?w.*d':
    ...         print('foo')
    ...
    foo

matchutils.AsSubstring

    compare case items using a substring match

    >>> match AsSubstring('hello world'):
    ...     case 'ell':
    ...         print('foo')
    ...
    foo

'''


from matchutils._matchutils import AsPattern, AsRegex, AsSubstring
