#!/usr/bin/env python3

from setuptools import setup

setup(
    name='matchutils',
    version='0.0.1',
    description='extensions for python\'s match/case statement',
    long_description='Check out the README on [GitLab](https://gitlab.com/thomasjlsn/matchutils)!',
    long_description_content_type='text/markdown',
    author='Thomas Ellison',
    author_email='thomasjlsn@gmail.com',
    url='https://gitlab.com/thomasjlsn/matchutils',
    packages=['matchutils'],
    license_files=['LICENSE'],
    package_data={
        'matchutils': ['py.typed'],  # Indicates to mypy that modules are typed.
    },
)
